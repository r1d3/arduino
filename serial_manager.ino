/*
  # @author: Arnaud Joset
  #
  # This file is part of Agayon: R1D3.
  #
  # R1D3 is free software: you can redistribute it and/or modify
  # it under the terms of the GNU General Public License as published by
  # the Free Software Foundation, either version 3 of the License, or
  # (at your option) any later version.
  #
  # R1D3 is distributed in the hope that it will be useful,
  # but WITHOUT ANY WARRANTY; without even the implied warranty of
  # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  # GNU General Public License for more details.
  #
  # You should have received a copy of the GNU General Public License
  # along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
  #
*/

void speed_manager(String command) {
  // String contains s= wich is the code for speed management
  command.replace("s=", "");
  unsigned int val = command.toInt();
  switch (val) {
    case 1:
      max_speed += 10;
#ifdef MATRIX
      matrix.clear();
      matrix.drawBitmap(0, 0, speedup_bmp, 8, 8, LED_RED);
      matrix.writeDisplay();
      delay(100);
#endif
      break;
    case 0:
      max_speed -= 10;
#ifdef MATRIX
      matrix.clear();
      matrix.drawBitmap(0, 0, speeddown_bmp, 8, 8, LED_GREEN);
      matrix.writeDisplay();
      delay(100);
#endif
      break;
  }
  middle_speed = max_speed / 2;
  if (middle_speed < 0) {
    middle_speed = 0;
  }
  if (middle_speed > 255) {
    middle_speed = 255;
  }
}

void lidar_manager(String command) {
  // String contains l= wich is the code for lidar management
  command.replace("l=", "");
  unsigned int val = command.toInt();
  switch (val) {
    case 0:
      lidar = false;
      rpi_halt = false;
      break;
    case 1:
      lidar = true;
      rpi_halt = true;
      break;
    case 2:
      rpi_halt = true;
  }
}

void mode_manager_inc(String command) {
  // String contains mm= wich is the code for mode management
  command.replace("mm=", "");
  unsigned int val = command.toInt();
  switch (val) {
    case 0:
      mode -= 1;
      mode_printer();
      break;
    case 1:
      mode += 1;
      mode_printer();
      break;
  }
  if (mode < 0) {
    mode = 0;
  }
  if (mode > 5) {
    mode = 5;
  }
}

void mode_manager(String command) {
  // String contains m= wich is the code for mode management
  command.replace("m=", "");
  unsigned int val = command.toInt();
  switch (val) {
    case 0:
      mode = 0;
      mode_printer();
      break;
    case 1:
      mode = 1;
      mode_printer();
      break;
    case 2:
      mode = 2;
      mode_printer();
      break;
    case 3:
      mode = 3;
      mode_printer();
      break;
    case 4:
      mode = 4;
      mode_printer();
      break;
    case 5:
      mode = 5;
      mode_printer();
      break;
  }
  if (mode < 0) {
    mode = 0;
  }
  if (mode > 5) {
    mode = 5;
  }
}

void remote_manager(String command) {

  timer = millis();
  // String contains r= wich is the code for remote control
  command.replace("r=", "");
  unsigned int val = command.toInt();
  if (val != 5) {
    // prevent setting mode = 1 when we are just releasing a key and r=5 is sent
    mode = 2; // todo: handle case with mode = 2
  }
  
  switch (val) {
    case 1:
      forward_smooth_remote();
      break;
    case 2:
      //backward_smooth();
      backward_smooth_remote();
      break;
    case 3:
      // right
      hard_turn_remote(true);
      break;
    case 4:
      // left
      hard_turn_remote(false);
      break;
    case 5:
      stop_motor_smooth();
      break;
    case 6:
      stop_motor_smooth();
      break;
    default:
      // do nothing
      stop_motor();
      break;
      timer_remote = millis();
  }

}

void small_angle_manager(String command) {
  timer = millis();
  // String contains r= wich is the code for remote control
  command.replace("a=", "");
  unsigned int val = command.toInt();
  switch (val) {
    case 1:
      // right
      soft_turn_remote(true);
      break;
    case 2:
      // left
      soft_turn_remote(false);
      break;
    default:
      // do nothing
      //          stop_motor();
      break;
      RPI_PRINT("DIR ");
      RPI_PRINTLN(dir);
      //encoder_manager();
      delay(100);
      timer_remote = millis();
  }

}



void serial_manager(String command) {
  RPI_PRINT("SERIAL MANAGER ");
  RPI_PRINTLN(command);  // todo: REMOVE LATER
  //TODO: take into account the values of the ultrasonic sensors. Prevent it to have collisions.
  if (command.indexOf("r=") == 0 && (millis() - timer >= 25))
  {
    remote_manager(command);
  }
  if (command.indexOf("m=") == 0 && rpi_halt == false) {
    // the current mode must be changed
    mode_manager(command);
  }
  if (command.indexOf("mm=") == 0 && rpi_halt == false) {
    // the current mode must be changed
    mode_manager_inc(command);
  }

  if (command.indexOf("s=") == 0 && rpi_halt == false) {
    // the current speed must be changed
    speed_manager(command);
  }
  if (command.indexOf("d=") == 0 && rpi_halt == false) {
    // raspberry pi request the odometry data
    stop_motor_smooth();
    encoder_manager();
    minimu_manager_avg(5000);
  }
  if (command.indexOf("a=") == 0 && rpi_halt == false) {
    // raspberry pi request small angles
    small_angle_manager(command);
  }
  if (command.indexOf("l=") == 0) {
    // raspberry pi request to turn on the odometry data when robot stops
    lidar_manager(command);
  }
  //FIXME
  if (command.indexOf("plop") == 0) {
    // print odometry data
    RPI_PRINT("CU:ac_");
    RPI_PRINTLN(millis());
    encoder_manager();
    check_sides(true);
    check_front(true);
    check_back(true);
    RPI_PRINT("CU:ac_-1");
  }
  DEBUG_PRINT(command);
  command = "";
}

void minimu_to_RPI()
{
  RPI_PRINTLN("");
  DEBUG_PRINTLN("");
  String output = "";
#if PRINT_EULER == 1
  output += "ANG:";
  output += ToDeg(roll);
  output += ",";
  output += ToDeg(pitch);
  output +=  ",";
  output += ToDeg(yaw);
  RPI_PRINTLN(output);
  DEBUG_PRINTLN(output);
#endif
#if PRINT_ANALOGS==1
  output = "";
  output += ",AN";
  output += AN[0] ;//(int)read_adc(0)
  output += ",";
  output += AN[1];
  output += ",";
  output += AN[2];
  output += ",";
  output += AN[3];
  output += ",";
  output += AN[4];
  output += ",";
  output += AN[5];
  output += ",";
  output += c_magnetom_x;
  output += ",";
  output += c_magnetom_y;
  output += ",";
  output += c_magnetom_z;
  output += ",";

  RPI_PRINT(output);
  DEBUG_PRINT(output);

#endif
  /*#if PRINT_DCM == 1
    Serial.print (",DCM:");
    Serial.print(convert_to_dec(DCM_Matrix[0][0]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[0][1]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[0][2]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[1][0]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[1][1]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[1][2]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[2][0]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[2][1]));
    Serial.print (",");
    Serial.print(convert_to_dec(DCM_Matrix[2][2]));
    #endif*/
  RPI_PRINTLN();

}

long convert_to_dec(float x)
{
  return x * 10000000;
}
