/*
  # @author: Arnaud Joset
  #
  # This file is part of Agayon: R1D3.
  #
  # R1D3 is free software: you can redistribute it and/or modify
  # it under the terms of the GNU General Public License as published by
  # the Free Software Foundation, either version 3 of the License, or
  # (at your option) any later version.
  #
  # R1D3 is distributed in the hope that it will be useful,
  # but WITHOUT ANY WARRANTY; without even the implied warranty of
  # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  # GNU General Public License for more details.
  #
  # You should have received a copy of the GNU General Public License
  # along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
  #
*/

void sides_checker() {
  // SIDES
  distance_ret = check_sides(verbose_dist);
  // we have place on the right, turn right
  // check_sides ()return true if the robot has place on the right;
  // return false if the robot on the left;
  if (distance_ret == 0 && min_distance > 0) {
    RDEBUG_PRINTLN("4 LEFT OK");
#ifdef MATRIX
    matrix.clear();
    matrix.drawBitmap(0, 0, left_bmp, 8, 8, LED_YELLOW);
    matrix.writeDisplay();
#endif
    RDEBUG_PRINTLN("2 HARD TURN LEFT");
    hard_turn_angle(false, quarter);

  }
  // we have place on the left, turn left
  else if (distance_ret == 1 && min_distance > 0) {
    RDEBUG_PRINTLN("5 RIGHT OK");
#ifdef MATRIX
    matrix.clear();
    matrix.drawBitmap(0, 0, right_bmp, 8, 8, LED_YELLOW);
    matrix.writeDisplay();
    RDEBUG_PRINTLN("3 HARD TURN RIGHT");
    hard_turn_angle(true, quarter);

#endif
  }
}

void explorator() {
  DEBUG_PRINTLN("explo");
  RDEBUG_PRINTLN("MOD_EXP");

  while (mode == 3) {
    // it is called after each loop normally but as we are not in a loop, we need to check the avaibility of serial data.
    //serialEventRun() ;
    if (stringComplete) {
      // Serial manager
      serial_manager(inputString);
      inputString = "";
      stringComplete = false;
    }
    if (Serial3.available()) {
      serialEvent3();
    }
    if ((millis() - timer) >= 40) // Main loop runs at XXHz
    {
      counter++;
      encoder_counter++;
      timer = millis();

      if (counter > 2)  // Read captors data at YYHz... (n loop runs)
      {

        RDEBUG_PRINT("DIR MOV");
        RDEBUG_PRINT(dir);
        RDEBUG_PRINTLN(moving);

        // If we are moving forward, measure what is in front of us
        // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.
        if (dir == false) {
          forward_smooth();
        }

        else {// measure distance back, we are going backward
          backward_smooth();
          // We are going backward for more than 800 ms
          if (millis() - backward_timer > 600) {
            backward_timer = millis();
            // We need to go forward now. We ensure to do it right now.
            sides_checker();
            dir = false;
            counter = 42;
            timer = 0;
            stop_motor_smooth();
          }
        }

        if (min_distance == 0) {
          RPI_PRINTLN(" Ultrasonic fail: distance 0 ");
          DEBUG_PRINTLN(" Ultrasonic fail: distance 0 ");
#ifdef MATRIX
          matrix.clear();
          matrix.drawBitmap(0, 0, error_bmp, 8, 8, LED_RED);
          matrix.writeDisplay();  // write the changes we just made to the display
          stop_motor_smooth();
          delay(1000);
#endif
        }
        sides_checker();
        delay(30);

        if (encoder_counter > 10000) {
          //encoder_manager();
          //encoder_counter = 0;
          //minimu_manager_avg(10);
        }
        // End counter : 2
      }
      // end millis loop
    }
    //end infinite loop
  }
}

// this one is useless, remote controll: just do what the serial says :-)
//void remote_control() {
//  while (mode == 1 || mode == 2) {
//    if ((millis() - timer) >= 40) // Main loop runs at XXHz
//    {
//      counter++;
//      encoder_counter++;
//      timer = millis();
//      distance_ret = check_sides(verbose_dist);
//      if (distance_ret != 2) {
//        stop_motor_smooth();
//#ifdef MATRIX
//        matrix.clear();
//        matrix.drawBitmap(0, 0, obstacles_bmp, 8, 8, LED_RED);
//        matrix.writeDisplay();  // write the changes we just made to the display
//        stop_motor_smooth();
//        delay(1000);
//#endif
//      }
//
//      if (min_distance == 0) {
//        RPI_PRINTLN(" Ultrasonic fail: distance 0 ");
//        DEBUG_PRINTLN(" Ultrasonic fail: distance 0 ");
//#ifdef MATRIX
//        matrix.clear();
//        matrix.drawBitmap(0, 0, error_bmp, 8, 8, LED_RED);
//        matrix.writeDisplay();  // write the changes we just made to the display
//        stop_motor_smooth();
//        delay(1000);
//#endif
//      }
//      if ((millis() - timer_remote )>= 600 && mode == 1) {
//        // no order since too much time. We stop
//        stop_motor_smooth();
//      }
//    }
//  }
//}

void demo() {
  RPI_PRINTLN("MOD_DEM");
  DEBUG_PRINTLN("INIT");
  init_matrix();
  DEBUG_PRINTLN("FACES");
  faces();
  DEBUG_PRINTLN("COUNT");
  countdown();
  DEBUG_PRINTLN("GEOMETRY");
  geometry();
  DEBUG_PRINTLN("TEXT");
  make_text();
  delay(1000);
}

/*  TESTS */

void sides_assurance() {
  // todo
}



void motor_test() {
  RPI_PRINTLN("MOD_MOT");
  DEBUG_PRINTLN("motor test");
  boolean ret = false;
  while (ret == false) {
    // il tourne pas à gauche
    DEBUG_PRINTLN("STOP");
    stop_motor_smooth();
    delay(200);
    DEBUG_PRINTLN("FORWARD");
    forward(10, 200);
    delay(300);
    DEBUG_PRINTLN("4 TURN RIGHT");
    // hard_turn(true); FIXME
    delay(300);
    DEBUG_PRINTLN("5 TURN LEFT");
    //hard_turn(false); FIXME
    delay(300);
    stop_motor_smooth();
    delay(300);
    DEBUG_PRINTLN("BACKWARD");
    //  Serial.println("backward");
    backward(10, 200);
    delay(300);
  }
}



void ultrasonic_test() {
  RPI_PRINTLN("MOD_ultrasonic test");
  distance_ret = check_front(verbose_dist);
  DEBUG_PRINT(" ===> Front ");
  DEBUG_PRINTLN (distance_ret);
  delay(1000);
  distance_ret = check_sides(verbose_dist);
  DEBUG_PRINT(" ==> Sides ");
  DEBUG_PRINTLN(distance_ret);
  delay(1000);
  distance_ret = check_back(verbose_dist);
  DEBUG_PRINT(" ==> back ");
  DEBUG_PRINTLN(distance_ret);
  delay(1000);
  DEBUG_PRINTLN("-------------------");
  delay(2000);
}

void minimu_test() {

  if ((millis() - timer) >= 100) // Main loop runs at XXHz
  {
    counter++;
    timer_minimu = timer;
    timer = millis();
    G_Dt = (timer - timer_minimu) / 1000.0; // Real time of loop run. We use this on the DCM algorithm (gyro integration time)
    // *** DCM algorithm
    // Data adquisition
    Read_Gyro();   // This read gyro data
    Read_Accel();     // Read I2C accelerometer

    if (counter > 5)  // Read compass data at 10Hz... (5 loop runs)
    {
      counter = 0;
      Read_Compass();    // Read I2C magnetometer
      Compass_Heading(); // Calculate magnetic heading
    }

    // Calculations...
    Matrix_update();
    Normalize();
    Drift_correction();
    Euler_angles();
    // ***
    minimu_to_RPI();
  }



}

void rotation_imu_test() {

  RPI_PRINTLN("");
  // turn 180 right

  //hard_turn_angle(true, 180) ;
  RPI_PRINT("  RIGHT  -->  ");
  //test_turn_angle(true);
  hard_turn_angle(true, 180);

  delay(1000);
  // turn 90 left
  //hard_turn_angle(false, 90) ;
  RPI_PRINTLN("");
  RPI_PRINT("  LEFT  ---> ");
  //test_turn_angle(false);
  hard_turn_angle(false, 180);

  delay(3000);
  // test
  RightEnc.write(0);
  RightEnc.write(0);



}
