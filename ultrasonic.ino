/*
  # @author: Arnaud Joset
  #
  # This file is part of Agayon: R1D3.
  #
  # R1D3 is free software: you can redistribute it and/or modify
  # it under the terms of the GNU General Public License as published by
  # the Free Software Foundation, either version 3 of the License, or
  # (at your option) any later version.
  #
  # R1D3 is distributed in the hope that it will be useful,
  # but WITHOUT ANY WARRANTY; without even the implied warranty of
  # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  # GNU General Public License for more details.
  #
  # You should have received a copy of the GNU General Public License
  # along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
  #
*/

bool check_back(bool verbose) {
  delay(30);
  int d_rr = measure_distance(sonar_RR);
  DEBUG_PRINT("d_rr");
  DEBUG_PRINT(d_rr);
  if (verbose) {
    RPI_PRINT(",UL:drr_");
    RPI_PRINT(d_rr);
  }
  min_distance = d_rr;
  if (d_rr < limit_distance) {
    return false;
  }
  return true;
}

// todo: make a mute version of this one
bool check_front(bool verbose) {
  //delay(30);
  //int d_fc = measure_distance(sonar_F_C);
  int d_fc = 0;
  delay(30);
  int d_fl = measure_distance(sonar_F_L);
  delay(30);
  int d_fr = measure_distance(sonar_F_R);

  DEBUG_PRINT(d_fc);
  DEBUG_PRINT(" fl ");
  DEBUG_PRINT(d_fl);
  DEBUG_PRINT(" fr ");
  DEBUG_PRINT(d_fr);
  if (verbose) {
    RPI_PRINT(",UL:fc_");
    RPI_PRINT(d_fc);
    RPI_PRINT(":fl_");
    RPI_PRINT(d_fl);
    RPI_PRINT(":fr_");
    RPI_PRINT(d_fr);
  }
  //  if ((d_fc < limit_distance) || (d_fr < limit_distance) || (d_fc > limit_ground)) {
  // Limit ground does not work.
  min_distance = min(d_fl, d_fr);
  if ((d_fl < limit_distance) || (d_fr < limit_distance)) {
    return false;
  }
  return true;
}

// todo: make a mute version of this one
byte check_sides(bool verbose) {
  // measure distance on the sides
  delay(30);
  int d_r = measure_distance(sonar_R);
  delay(30);
  int d_l = measure_distance(sonar_L);
  DEBUG_PRINT("d_r ");
  DEBUG_PRINT(d_r);
  DEBUG_PRINT(" d_l ");
  DEBUG_PRINT(d_l);
  if (verbose) {
    RPI_PRINT(",UL:dr_");
    RPI_PRINT(d_r);
    RPI_PRINT(":dl_");
    RPI_PRINT(d_l);
  }
  // obstacle on the right: turn left

  //      return true if the robot has place on the right;
  //      return false if the robot on the left;

  min_distance = min(d_r , d_l);
  if (d_r > min_sides && d_l > min_sides ) {
    return 2;
  } else {
    if (d_r > d_l) {
      return 1;
    }
    return 0;
  }
}



void distance_calculate(NewPing sonar, String captor) {
  int mu = sonar.ping_median();
  int d = sonar.convert_cm(mu);
  Serial.print(captor);
  Serial.print(" : " );
  Serial.print(d);
  Serial.println("cm");

}

int measure_distance(NewPing sonar) {
  int mu = 0;
  int trials = 0;
  do {
    mu = sonar.ping_median();
    //DEBUG_PRINTLN(mu);
    delay(30);
    trials += 1;
    if (trials > 15) {
      RPI_PRINTLN(",ERROR ulrasonic,CU:ac_-1");
      stop_motor_smooth();
#ifdef MATRIX
      matrix.clear();
      matrix.drawBitmap(0, 0, error_bmp, 8, 8, LED_RED);
      matrix.writeDisplay();  // write the changes we just made to the display
      delay(1000);
#endif
      break;
    }
  }
  while (mu == 0);
  int distance_cm = sonar.convert_cm(mu);
  return distance_cm;
}
