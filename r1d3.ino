/*
  # @author: Arnaud Joset
  #
  # This file is part of Agayon: R1D3.
  #
  # R1D3 is free software: you can redistribute it and/or modify
  # it under the terms of the GNU General Public License as published by
  # the Free Software Foundation, either version 3 of the License, or
  # (at your option) any later version.
  #
  # R1D3 is distributed in the hope that it will be useful,
  # but WITHOUT ANY WARRANTY; without even the implied warranty of
  # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  # GNU General Public License for more details.
  #
  # You should have received a copy of the GNU General Public License
  # along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
  #
*/

#define ENCODER_OPTIMIZE_INTERRUPTS
#include <Encoder.h>
#include <Wire.h>
#include <NewPing.h>

#include <Adafruit_GFX.h>
#include "Adafruit_LEDBackpack.h"

Adafruit_BicolorMatrix matrix = Adafruit_BicolorMatrix();

#define MAX_DISTANCE 400 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
// Front right
#define PIN_F_R  33  // Arduino pin tied to trigger pin on the ultrasonic sensor.
// Front Center
#define PIN_F_C  37  // Arduino pin tied to trigger pin on the ultrasonic sensor.
// Front Left
#define PIN_F_L  22 // Arduino pin tied to trigger pin on the ultrasonic sensor.
// LEFT
#define PIN_L  44  // Arduino pin tied to trigger pin on the ultrasonic sensor.
// Right
#define PIN_R  35  // Arduino pin tied to trigger pin on the ultrasonic sensor.
// Rear
#define PIN_RR  39  // Arduino pin tied to trigger pin on the ultrasonic sensor.

// Comment to disable debug and the debug code won't even be compiled :-)
//#define DEBUG
#define RPI
//#define RDEBUG
#define MATRIX
#define MINIMU

#ifdef DEBUG
#define DEBUG_PRINT(x)     Serial.print(x)
#define DEBUG_PRINTDEC(x)  Serial.print(x, DEC)
#define DEBUG_PRINTLN(x)   Serial.println(x)
#else
#define DEBUG_PRINT(x)
#define DEBUG_PRINTDEC(x)
#define DEBUG_PRINTLN(x)
#endif

#ifdef RPI
#define RPI_PRINT(x)     Serial3.print(x)
#define RPI_PRINTDEC(x)  Serial3.print(x, DEC)
#define RPI_PRINTLN(x)   Serial3.println(x)
#define RPI_AVAIL()      Serial3.available()
#define RPI_READ()       Serial3.read()
#else
#define RPI_PRINT(x)
#define RPI_PRINTDEC(x)
#define RPI_PRINTLN(x)
#endif
#ifdef RDEBUG
#define RDEBUG_PRINT(x)     Serial3.print(x)
#define RDEBUG_PRINTLN(x)   Serial3.println(x)
#else
#define RDEBUG_PRINT(x)
#define RDEBUG_PRINTLN(x)
#endif


String inputString = "";         // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete

NewPing sonar_F_R(PIN_F_R, PIN_F_R, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
NewPing sonar_F_C(PIN_F_C, PIN_F_C, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
NewPing sonar_F_L(PIN_F_L, PIN_F_L, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
NewPing sonar_L(PIN_L, PIN_L, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
NewPing sonar_R(PIN_R, PIN_R, MAX_DISTANCE); // NewPing setup of pins and maximum distance.
NewPing sonar_RR(PIN_RR, PIN_RR, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

static const uint8_t PROGMEM
backward_bmp[] =
{
  B00000000,
  B00001000,
  B00011100,
  B00111110,
  B01111111,
  B00011100,
  B00011100,
  B00011100
},
forward_bmp[] {
  B00000000,
  B00011100,
  B00011100,
  B00011100,
  B01111111,
  B00111110,
  B00011100,
  B00001000
},
left_bmp[] {
  B00000000,
  B00001000,
  B00001100,
  B01111110,
  B01111111,
  B01111110,
  B00001100,
  B00001000
},
right_bmp[] {
  B00000000,
  B00001000,
  B00011000,
  B00111111,
  B01111111,
  B00111111,
  B00011000,
  B00001000
},
random_bmp[] {
  B00000000,
  B00111100,
  B01100110,
  B00000110,
  B00011100,
  B00011000,
  B00000000,
  B00011000
},
remote_bmp[] {
  B00000000,
  B00000010,
  B00000010,
  B00001010,
  B00001010,
  B00101010,
  B00101010,
  B10101010
},
slam_bmp[] {
  B11111111,
  B10000001,
  B10000001,
  B10000001,
  B10110001,
  B10110001,
  B10000001,
  B11111111
};



// https://www.pjrc.com/teensy/td_libs_Encoder.html
// Change these two numbers to the pins connected to your encoder.
//   Best Performance: both pins have interrupt capability
//   Good Performance: only the first pin has interrupt capability
//   Low Performance:  neither pin has interrupt capability
Encoder RightEnc(2, 3);
Encoder LeftEnc(18, 19);

byte distance_ret ;
long position_right ;
long position_left;
long position_right_old;
long position_left_old;
long position_;
long position_old;
unsigned int encoder_counter = 0;
int min_sides = 100; // minimum distance on sides
int min_front_back = 100; // minimum distance front and back

// number of tick returned by the motor encoder per rotation.
float tick_per_rot = 6533.0;

//   avoid using pins with LEDs attached
int pwm_pin_right = 11;
int dir_pin_right = 53;
int pwm_pin_left = 9;
int dir_pin_left = 10;
// max speed depends on the size of the room.
// 0 < max speed <
int max_speed = 120;
int middle_speed = max_speed / 2;
int speed_v;
//long time_motor = 0; //general purpuse timer


unsigned long timer = 0; //general purpuse timer
unsigned long timer24 = 0; //Second timer used to print values
unsigned int counter = 0;
unsigned long backward_timer = 0; //timer indicating we are moving backward
unsigned long timer_minimu = 0;
unsigned long timer_remote = 0;
unsigned int val_old = 999;
//unsigned long remote_timer = 0; //
//unsigned long remote_timer_old = 0; //

// forward or backward; forward = false
bool dir = LOW;
bool moving = false;
bool verbose_dist = false;

int limit_distance = 90;
int limit_ground = 40;
int limit_side = 50;
int min_distance = 0;

//  These are the experimental constants used to turn the robot of predefined angles.
int eighth = 45;
int quarter = 180; // wheels must turn 180° for the robot to turn 90°
int two_third = 270; //to verify
int halft = 360; // to verify
int three_quarter = 540; // // to verify

int led_status = 13;
int led_com = 51;

int mode = 0;
bool lidar = false;
bool rpi_halt = false;
long int rpi_timeout;

/* MINIMU v3*/

// LSM303 accelerometer: 8 g sensitivity
// 3.9 mg/digit; 1 g = 256
#define GRAVITY 256  //this equivalent to 1G in the raw data coming from the accelerometer 

#define ToRad(x) ((x)*0.01745329252)  // *pi/180
#define ToDeg(x) ((x)*57.2957795131)  // *180/pi

// L3G4200D gyro: 2000 dps full scale
// 70 mdps/digit; 1 dps = 0.07
#define Gyro_Gain_X 0.07 //X axis Gyro gain
#define Gyro_Gain_Y 0.07 //Y axis Gyro gain
#define Gyro_Gain_Z 0.07 //Z axis Gyro gain
#define Gyro_Scaled_X(x) ((x)*ToRad(Gyro_Gain_X)) //Return the scaled ADC raw data of the gyro in radians for second
#define Gyro_Scaled_Y(x) ((x)*ToRad(Gyro_Gain_Y)) //Return the scaled ADC raw data of the gyro in radians for second
#define Gyro_Scaled_Z(x) ((x)*ToRad(Gyro_Gain_Z)) //Return the scaled ADC raw data of the gyro in radians for second

// LSM303 magnetometer calibration constants; use the Calibrate example from
// the Pololu LSM303 library to find the right values for your board
#define M_X_MIN -3544// -421
#define M_Y_MIN 2544 // -639
#define M_Z_MIN -3452 // -238
#define M_X_MAX 2510// 424
#define M_Y_MAX -3321// 295
#define M_Z_MAX 2883 // 472

#define Kp_ROLLPITCH 0.02
#define Ki_ROLLPITCH 0.00002
#define Kp_YAW 1.2
#define Ki_YAW 0.00002

/*For debugging purposes*/
//OUTPUTMODE=1 will print the corrected data,
//OUTPUTMODE=0 will print uncorrected data of the gyros (with drift)
#define OUTPUTMODE 1

//#define PRINT_DCM 0     //Will print the whole direction cosine matrix
#define PRINT_ANALOGS 0 //Will print the analog raw data
#define PRINT_EULER 1   //Will print the Euler angles Roll, Pitch and Yaw


// Uncomment the below line to use this axis definition:
// X axis pointing forward
// Y axis pointing to the right
// and Z axis pointing down. -->  silkscreen is on top
// Positive pitch : nose up
// Positive roll : right wing down
// Positive yaw : clockwise
int SENSOR_SIGN[9] = {1, 1, 1, -1, -1, -1, 1, 1, 1}; //Correct directions x,y,z - gyro, accelerometer, magnetometer
// Uncomment the below line to use this axis definition:
// X axis pointing forward
// Y axis pointing to the left
// and Z axis pointing up.
// Positive pitch : nose down
// Positive roll : right wing down
// Positive yaw : counterclockwise
//int SENSOR_SIGN[9] = {1,-1,-1,-1,1,1,1,-1,-1}; //Correct directions x,y,z - gyro, accelerometer, magnetometer

float G_Dt = 0.02;  // Integration time (DCM algorithm)  We will run the integration loop at 50Hz if possible

int AN[6]; //array that stores the gyro and accelerometer data
int AN_OFFSET[6] = {0, 0, 0, 0, 0, 0}; //Array that stores the Offset of the sensors

int gyro_x;
int gyro_y;
int gyro_z;
int accel_x;
int accel_y;
int accel_z;
int magnetom_x;
int magnetom_y;
int magnetom_z;
float c_magnetom_x;
float c_magnetom_y;
float c_magnetom_z;
float MAG_Heading;

float Accel_Vector[3] = {0, 0, 0}; //Store the acceleration in a vector
float Gyro_Vector[3] = {0, 0, 0}; //Store the gyros turn rate in a vector
float Omega_Vector[3] = {0, 0, 0}; //Corrected Gyro_Vector data
float Omega_P[3] = {0, 0, 0}; //Omega Proportional correction
float Omega_I[3] = {0, 0, 0}; //Omega Integrator
float Omega[3] = {0, 0, 0};

// Euler angles
float roll;
float pitch;
float yaw;

float errorRollPitch[3] = {0, 0, 0};
float errorYaw[3] = {0, 0, 0};

byte gyro_sat = 0;

float DCM_Matrix[3][3] = {
  {
    1, 0, 0
  }
  , {
    0, 1, 0
  }
  , {
    0, 0, 1
  }
};
float Update_Matrix[3][3] = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}}; //Gyros here


float Temporary_Matrix[3][3] = {
  {
    0, 0, 0
  }
  , {
    0, 0, 0
  }
  , {
    0, 0, 0
  }
};

/* END MINIMU v3*/

void setup() {
  Serial.begin(2000000);
  DEBUG_PRINTLN("ready setup");
  Serial3.begin(2000000);
  RPI_PRINTLN("RPI LINK READY");
  delay(100);
#ifdef MATRIX
  matrix.begin(0x70);  // pass in the address
  matrix.clear();
  init_matrix();
#endif

#ifdef MINIMU
  Accel_Init();
  Compass_Init();
  Gyro_Init();

  delay(20);
  for (int i = 0; i < 32; i++) // We take some readings...
  {
    Read_Gyro();
    Read_Accel();
    for (int y = 0; y < 6; y++) // Cumulate values
      AN_OFFSET[y] += AN[y];
    delay(20);
  }

  for (int y = 0; y < 6; y++)
    AN_OFFSET[y] = AN_OFFSET[y] / 32;

  AN_OFFSET[5] -= GRAVITY * SENSOR_SIGN[5];

  DEBUG_PRINT("Offset:");
  for (int y = 0; y < 6; y++) {
    DEBUG_PRINT(AN_OFFSET[y]);
    DEBUG_PRINT(" ");
  }

#endif

  pinMode(dir_pin_right, OUTPUT);
  pinMode(pwm_pin_right, OUTPUT);
  pinMode(dir_pin_left, OUTPUT);
  pinMode(pwm_pin_left, OUTPUT);
  // remove this when RPI will be connected
  DEBUG_PRINTLN("Motors are ready");
  inputString.reserve(200);

  // pinMode(led_status, OUTPUT);
  pinMode(led_com, OUTPUT);

#ifdef RPI
  digitalWrite(led_com, HIGH);
#endif
  //digitalWrite(led_status, HIGH);

  //  delay(5000);

}

void loop() {
  //OVERRIDE OF THE MODE
  //DEBUG_PRINT("MODE ");
  //DEBUG_PRINTLN(mode);
  // mode 0 : do nothing but can be interupted by the remote
  // mode 1 : remote web page: no stop // broken so it's the same as mode 2 for now
  // mode 2 = remote playstation controller
  // mode 3 : explore
  // mode = 4 : demo
  if (stringComplete) {
    //    DEBUG_PRINTLN(inputString);
    serial_manager(inputString);
    inputString = "";
    stringComplete = false;
  }
  if (mode == 0) {
#ifdef MATRIX
    matrix.clear();
    init_matrix();
#endif
    delay(50);
  }
  if (mode == 2 ) {
#ifdef MATRIX
    matrix.clear();
    matrix.drawBitmap(0, 0, remote_bmp, 8, 8, LED_YELLOW);
    matrix.writeDisplay();
    // warning: we don't need delay here. It break the pleasure to remote control it
#endif
  }

  if (mode == 3) {
#ifdef MATRIX
    matrix.clear();
    matrix.drawBitmap(0, 0, random_bmp, 8, 8, LED_YELLOW);
    matrix.writeDisplay();
    delay(3000);
#endif
    max_speed = 160;
    middle_speed = max_speed / 2;
    explorator();
  }
  if (mode == 4) {
#ifdef MATRIX
    matrix.clear();
    matrix.drawBitmap(0, 0, random_bmp, 8, 8, LED_YELLOW);
    matrix.writeDisplay();
    delay(3000);
#endif
    demo();
  }
}


/*
  SerialEvent occurs whenever a new data comes in the hardware serial RX. This
  routine is run between each time loop() runs, so using delay inside loop can
  delay response. Multiple bytes of data may be available.
  https://forum.arduino.cc/index.php?topic=166650.0
*/
void serialEvent3() {
  char inChar;
  while (Serial3.available()) {
    // get the new byte:
    inChar = (char)Serial3.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    //    13 is the carriage return
    if (inChar == 13 || inChar == 10) {
      //DEBUG_PRINTLN(inputString);
      stringComplete = true;
    }
  }
}
