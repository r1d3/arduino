/*
  # @author: Arnaud Joset
  #
  # This file is part of Agayon: R1D3.
  #
  # R1D3 is free software: you can redistribute it and/or modify
  # it under the terms of the GNU General Public License as published by
  # the Free Software Foundation, either version 3 of the License, or
  # (at your option) any later version.
  #
  # R1D3 is distributed in the hope that it will be useful,
  # but WITHOUT ANY WARRANTY; without even the implied warranty of
  # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  # GNU General Public License for more details.
  #
  # You should have received a copy of the GNU General Public License
  # along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
  #
*/

/***************************************************
   Documentation from Adafruit:
  This is a library for our I2C LED Backpacks

  Designed specifically to work with the Adafruit LED Matrix backpacks
  ----> http://www.adafruit.com/products/872
  ----> http://www.adafruit.com/products/871
  ----> http://www.adafruit.com/products/870
  This library has been written by Limor Fried/Ladyada for Adafruit Industries.
  BSD license, all text above must be included in any redistribution
 ****************************************************/

static const PROGMEM byte DIGITS[][8] = {
  {
    B00000000,
    B00011000,
    B00011000,
    B00111000,
    B00011000,
    B00011000,
    B00011000,
    B01111110
  }, {
    B00000000,
    B00111100,
    B01100110,
    B00000110,
    B00001100,
    B00110000,
    B01100000,
    B01111110
  }, {
    B00000000,
    B00111100,
    B01100110,
    B00000110,
    B00011100,
    B00000110,
    B01100110,
    B00111100
  }, {
    B00000000,
    B00001100,
    B00011100,
    B00101100,
    B01001100,
    B01111110,
    B00001100,
    B00001100
  }, {
    B00000000,
    B01111110,
    B01100000,
    B01111100,
    B00000110,
    B00000110,
    B01100110,
    B00111100
  }, {
    B00000000,
    B00111100,
    B01100110,
    B01100000,
    B01111100,
    B01100110,
    B01100110,
    B00111100
  }, {
    B00000000,
    B01111110,
    B01100110,
    B00001100,
    B00001100,
    B00011000,
    B00011000,
    B00011000
  }, {
    B00000000,
    B00111100,
    B01100110,
    B01100110,
    B00111100,
    B01100110,
    B01100110,
    B00111100
  }, {
    B00000000,
    B00111100,
    B01100110,
    B01100110,
    B00111110,
    B00000110,
    B01100110,
    B00111100
  }, {
    B00000000,
    B00111100,
    B01100110,
    B01101110,
    B01110110,
    B01100110,
    B01100110,
    B00111100
  }
};




static const uint8_t PROGMEM
heart_bmp[] =
{
  B00000000,
  B00100010,
  B01110111,
  B01111111,
  B01111111,
  B00111110,
  B00011100,
  B00001000
},
bomb_bmp[] =
{
  B00011100,
  B00010000,
  B00111100,
  B01111110,
  B01111110,
  B01111110,
  B01111110,
  B00111100
},
boom1_bmp[] =
{
  B10010001,
  B01010010,
  B00011000,
  B00100100,
  B11100111,
  B00011000,
  B01001010,
  B10000001
},
boom2_bmp[] =
{
  B00000000,
  B00000000,
  B00100100,
  B00011000,
  B00011000,
  B00100100,
  B00000000,
  B00000000
},
init_bmp[] =
{
  B00000000,
  B00000001,
  B00000010,
  B00000100,
  B10001000,
  B01010000,
  B00100000,
  B00000000

},
error_bmp[] =
{
  B00000000,
  B00011000,
  B00111100,
  B00111100,
  B00011000,
  B00011000,
  B00000000,
  B00011000
},
obstacles_bmp[] =
{
  B00111100,
  B00111100,
  B11000011,
  B11000011,
  B11000011,
  B11000011,
  B00111100,
  B00111100
},
stop_bmp[] =
{
  B00000000,
  B01100011,
  B01100011,
  B00110110,
  B00011100,
  B00110110,
  B01100011,
  B01100011
},
speedup_bmp[] =
{
  B10000001,
  B11000011,
  B11100111,
  B11111111,
  B11000011,
  B11000011,
  B11000011,
  B11000011
},
speeddown_bmp[] =
{
  B11000011,
  B11000011,
  B11000011,
  B11000011,
  B11111111,
  B11100111,
  B11000011,
  B10000001
}
;


void init_matrix() {
  matrix.drawBitmap(0, 0, init_bmp, 8, 8, LED_GREEN);
  matrix.writeDisplay();
}



void countdown() {
#ifdef MATRIX
  matrix.clear();
#endif
  for (int8_t x = 8; x >= 6; x--) {
    DEBUG_PRINTLN(x);
#ifdef MATRIX
    matrix.clear();
    matrix.drawBitmap(0, 0, DIGITS[x], 8, 8, LED_GREEN);
    matrix.writeDisplay();
#endif
    delay(1000);
  }

  for (int8_t x = 5; x >= 3; x--) {
    DEBUG_PRINTLN(x);
#ifdef MATRIX
    matrix.clear();
    matrix.drawBitmap(0, 0, DIGITS[x], 8, 8, LED_YELLOW);
    matrix.writeDisplay();
#endif
    delay(1000);
  }

  for (int8_t x = 2; x >= 0; x--) {
    DEBUG_PRINTLN(x);
#ifdef MATRIX
    matrix.clear();
    matrix.drawBitmap(0, 0, DIGITS[x], 8, 8, LED_RED);
    matrix.writeDisplay();
#endif
    delay(1000);
  }
#ifdef MATRIX
  matrix.clear();
  matrix.drawBitmap(0, 0, bomb_bmp, 8, 8, LED_YELLOW);
  matrix.drawPixel(6, 0, LED_RED);
  matrix.writeDisplay();  // write the changes we just made to the display
#endif
  delay(500);
#ifdef MATRIX
  matrix.clear();
  matrix.drawBitmap(0, 0, boom1_bmp, 8, 8, LED_RED);
  matrix.drawBitmap(0, 0, boom2_bmp, 8, 8, LED_YELLOW);
  matrix.writeDisplay();  // write the changes we just made to the display
#endif

  delay(500);
#ifdef MATRIX
  matrix.clear();
  matrix.drawBitmap(0, 0, heart_bmp, 8, 8, LED_RED);
  matrix.writeDisplay();  // write the changes we just made to the display
#endif
  delay(500);
}

void geometry() {
#ifdef MATRIX
  matrix.clear();      // clear display
  matrix.drawPixel(0, 0, LED_GREEN);
  matrix.writeDisplay();  // write the changes we just made to the display
#endif
  delay(1000);
#ifdef MATRIX
  matrix.clear();
  matrix.drawLine(0, 0, 7, 7, LED_YELLOW);
  matrix.writeDisplay();  // write the changes we just made to the display
#endif
  delay(1000);
#ifdef MATRIX
  matrix.clear();
  matrix.drawRect(0, 0, 8, 8, LED_RED);
  matrix.fillRect(2, 2, 4, 4, LED_GREEN);
  matrix.writeDisplay();  // write the changes we just made to the display
#endif
  delay(1000);
#ifdef MATRIX
  matrix.clear();
  matrix.drawCircle(3, 3, 3, LED_YELLOW);
  matrix.writeDisplay();  // write the changes we just made to the display
#endif
  delay(1000);

}

static const uint8_t PROGMEM
smile_bmp[] =
{ B00111100,
  B01000010,
  B10100101,
  B10000001,
  B10100101,
  B10011001,
  B01000010,
  B00111100
},
neutral_bmp[] =
{ B00111100,
  B01000010,
  B10100101,
  B10000001,
  B10111101,
  B10000001,
  B01000010,
  B00111100
},
frown_bmp[] =
{ B00111100,
  B01000010,
  B10100101,
  B10000001,
  B10011001,
  B10100101,
  B01000010,
  B00111100
};


void faces() {
#ifdef MATRIX
  matrix.clear();
  matrix.drawBitmap(0, 0, smile_bmp, 8, 8, LED_GREEN);
  matrix.writeDisplay();
#endif
  delay(1000);
#ifdef MATRIX
  matrix.clear();
  matrix.drawBitmap(0, 0, neutral_bmp, 8, 8, LED_YELLOW);
  matrix.writeDisplay();
#endif
  delay(1000);
#ifdef MATRIX
  matrix.clear();
  matrix.drawBitmap(0, 0, frown_bmp, 8, 8, LED_RED);
  matrix.writeDisplay();
#endif
  delay(1000);
}


void make_text() {
#ifdef MATRIX
  matrix.setTextWrap(false);  // we dont want text to wrap so it scrolls nicely
  matrix.setTextSize(1);
  matrix.setTextColor(LED_GREEN);
  for (int8_t x = 7; x >= -115; x--) {
    matrix.clear();
    matrix.setCursor(x, 0);
    matrix.print("R1D3 is in the place");
    matrix.writeDisplay();
    delay(100);
  }
  matrix.setRotation(3);
  matrix.setTextColor(LED_RED);
  for (int8_t x = 7; x >= -124; x--) {
    matrix.clear();
    matrix.setCursor(x, 0);
    matrix.print("Make demo great again!");
    matrix.writeDisplay();
    delay(100);
  }
  matrix.setRotation(0);
  matrix.clear();
#endif

}
