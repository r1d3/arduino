/*
  # @author: Arnaud Joset
  #
  # This file is part of Agayon: R1D3.
  #
  # R1D3 is free software: you can redistribute it and/or modify
  # it under the terms of the GNU General Public License as published by
  # the Free Software Foundation, either version 3 of the License, or
  # (at your option) any later version.
  #
  # R1D3 is distributed in the hope that it will be useful,
  # but WITHOUT ANY WARRANTY; without even the implied warranty of
  # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  # GNU General Public License for more details.
  #
  # You should have received a copy of the GNU General Public License
  # along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
  #
*/

void forward_smooth() {
  RDEBUG_PRINTLN("1 DIR FALSE");
#ifdef MATRIX
  matrix.clear();
  matrix.drawBitmap(0, 0, forward_bmp, 8, 8, LED_GREEN);
  matrix.writeDisplay();
#endif
  // forward only if we were stopped. Else, juste continue
  if (moving == false) {
    forward(10, 300);
    moving = true;
  }

  // measure distance front
  distance_ret = check_front(verbose_dist);
  if (distance_ret == false && min_distance > 0) {
    RDEBUG_PRINT("2 FRONT_FALSE, GO BACK");
    DEBUG_PRINTLN("2 FRONT_FALSE, GO BACK");
    // obstacle in front of us or a gap
    stop_motor_smooth();
    distance_ret = check_back(verbose_dist);
    backward(20, 100);
  }
  // problems, we should get the distance in these functions
  else if (distance_ret == true && min_distance > 0) {
    RDEBUG_PRINTLN("FRONT OK");
    DEBUG_PRINTLN("FRONT OK");
    delay(50);
  }
}

void backward_smooth() {
  if (moving == true) {
    // we are moving forward, we need to stop before going backward
    if (dir == false) {
      stop_motor_smooth();
    }
    backward_timer = millis();
    backward(10, 100);
  }
  RDEBUG_PRINT("DIR TRUE");
#ifdef MATRIX
  matrix.clear();
  matrix.drawBitmap(0, 0, backward_bmp, 8, 8, LED_RED);
  matrix.writeDisplay();
#endif
  distance_ret = check_back(verbose_dist);
  if (distance_ret == false && min_distance > 0) {
#ifdef MATRIX
    RDEBUG_PRINT("9 BACKWARD KO");
    matrix.clear();
    matrix.drawBitmap(0, 0, left_bmp, 8, 8, LED_YELLOW);
    matrix.writeDisplay();
#endif
    hard_turn_angle(false, quarter);
    forward_smooth();
    delay(600);
  }
  else if (distance_ret == true && min_distance > 0) {
    RDEBUG_PRINT("10 BACKWARD OK");
    if (moving == false) {
      // we are already moving backward
      backward(20, 100);
    }
    delay(500);
    // Go forward again
    dir = true ;
  }

}

void forward(int harsh, unsigned int delta_t) {
  DEBUG_PRINTLN("FORWARD");
  RDEBUG_PRINTLN("FORWARD");
#ifdef MATRIX
  matrix.clear();
  matrix.drawBitmap(0, 0, forward_bmp, 8, 8, LED_GREEN);
  matrix.writeDisplay();
#endif
  dir = false;
  accelerate(harsh, delta_t);
  moving = true;
  backward_timer = 0;

}

void backward(int harsh, unsigned int delta_t) {
  DEBUG_PRINTLN("BACKWARD");
  RDEBUG_PRINTLN("BACKWARD");
#ifdef MATRIX
  matrix.clear();
  matrix.drawBitmap(0, 0, backward_bmp, 8, 8, LED_RED);
  matrix.writeDisplay();
#endif
  dir = true;
  accelerate(harsh, delta_t);
  moving = true;

}

void accelerate(int harsh, unsigned int delta_t) {
  DEBUG_PRINTLN("ACCELERATE");
  RDEBUG_PRINTLN("ACCELERATE");
  speed_v = 0;
  digitalWrite(dir_pin_right, dir);
  digitalWrite(dir_pin_left, dir);
  do {
    if ((millis() - timer) >= delta_t ) // Main loop runs at XXHz
    {
      timer = millis();
      speed_v += harsh;
      analogWrite(pwm_pin_right, speed_v);
      analogWrite(pwm_pin_left, speed_v);
      RPI_PRINTLN();
    }
  } while (speed_v < middle_speed);

}

void decelerate(int harsh, unsigned int delta_t) {
  speed_v = middle_speed;
  do {
    if ((millis() - timer) >= delta_t ) // Main loop runs at XXHz
    {
      timer = millis();
      speed_v -= harsh;
      analogWrite(pwm_pin_right, speed_v);
      analogWrite(pwm_pin_left, speed_v);
    }
  } while (speed_v > 0);
}

void stop_motor() {
  analogWrite(pwm_pin_right, 0);
  analogWrite(pwm_pin_left, 0);
  moving = false;
}

void stop_motor_smooth() {
  if (moving == true) {
    decelerate(20, 10);
  }
  analogWrite(pwm_pin_right, 0);
  analogWrite(pwm_pin_left, 0);
  delay(100);
  moving = false;
  //  RPI_PRINT("LIDAR VAL");
  //  RPI_PRINTLN(lidar);
  if (lidar == true) {
    // Send encoders, distance etc
    position_right = RightEnc.read();
    position_left = LeftEnc.read();
    timer = millis();
    sensors_to_rpi();
    lidar = false;
    rpi_halt = false;
  }
}
void hard_turn(bool right, int rotation_speed) {

  if (right == true) {
    // turn right
#ifdef MATRIX
    matrix.clear();
    matrix.drawBitmap(0, 0, right_bmp, 8, 8, LED_YELLOW);
    matrix.writeDisplay();
#endif
    digitalWrite(dir_pin_right, HIGH);
    digitalWrite(dir_pin_left, LOW);
    //speed_v = 0;
    speed_v = rotation_speed;
    analogWrite(pwm_pin_right, speed_v);
    analogWrite(pwm_pin_left, speed_v);
  }
  else
    // turn left
  {
#ifdef MATRIX
    matrix.clear();
    matrix.drawBitmap(0, 0, left_bmp, 8, 8, LED_YELLOW);
    matrix.writeDisplay();
#endif
    digitalWrite(dir_pin_right, LOW);
    digitalWrite(dir_pin_left, HIGH);
    speed_v = rotation_speed;
    analogWrite(pwm_pin_right, speed_v);
    analogWrite(pwm_pin_left, speed_v);

  }

}


void soft_turn(bool right, int rotation_speed) {

  if (right == true) {
    // turn right
#ifdef MATRIX
    matrix.clear();
    matrix.drawBitmap(0, 0, right_bmp, 8, 8, LED_GREEN);
    matrix.writeDisplay();
#endif
    digitalWrite(dir_pin_right, HIGH);
    digitalWrite(dir_pin_left, LOW);
    //speed_v = 0;
    speed_v = rotation_speed;
    analogWrite(pwm_pin_right, 0);
    analogWrite(pwm_pin_left, speed_v);
  }
  else
    // turn left
  {
#ifdef MATRIX
    matrix.clear();
    matrix.drawBitmap(0, 0, left_bmp, 8, 8, LED_GREEN);
    matrix.writeDisplay();
#endif
    digitalWrite(dir_pin_right, LOW);
    digitalWrite(dir_pin_left, HIGH);
    speed_v = rotation_speed;
    analogWrite(pwm_pin_right, speed_v);
    analogWrite(pwm_pin_left, 0);

  }

}



void hard_turn_angle(bool right, int angle) {

  /*
    When we turn RIGHT: endoder RIGHT  decreases
                RIGHT          LEFT   decreases
    When we turn LEFT : encoder RIGHT increases
                LEFT           LEFT  increases

    We canculate the difference between the tick we need to reach and the current tick.
    When this difference is lower than a threshold, we stop turning.
    It depends on the accuracy of the encoders. Angles are not real angles. 180 --> ~ 90°

  */

  DEBUG_PRINT("HARD_TURN ANGLE ");
  DEBUG_PRINT(right);
  DEBUG_PRINTLN(angle);
  position_right_old = LeftEnc.read();
  position_left_old = RightEnc.read();

  long int stop_pos_right;
  long int stop_pos_left;
  long int delta_right;
  long int delta_left;
  int delta_right_prev = tick_per_rot; //
  int delta_left_prev = tick_per_rot; // at max angle = 360° and delta is the numbner of tick per turn
  bool turn_loop = true;


  if (right == false) {
    stop_pos_right = position_right_old - angle_to_tick(angle);
    stop_pos_left = position_right_old - angle_to_tick(angle);
  } else {
    stop_pos_right = position_right_old + angle_to_tick(angle);
    stop_pos_left = position_right_old + angle_to_tick(angle);
  }



  do {
    hard_turn(right, middle_speed);
    position_right = RightEnc.read();
    position_left = LeftEnc.read();
    delta_right = abs(position_right - stop_pos_right);
    delta_left  = abs(position_left  - stop_pos_left);

    if (delta_right < 100 || delta_left < 100) {
      turn_loop = false;
    }

  } while (turn_loop == true);
  stop_motor_smooth();

}


void soft_turn_angle(bool right, int angle) {

  /*
    When we turn RIGHT: endoder RIGHT  do nothing
                RIGHT          LEFT   decreases
    When we turn LEFT : encoder RIGHT increases
                LEFT           LEFT  do nothing

    We canculate the difference between the tick we need to reach and the current tick.
    When this difference is lower than a threshold, we stop turning.
    It depends on the accuracy of the encoders. Angles are not real angles. 180 --> ~ 90°

  */

  DEBUG_PRINT("SOFT_TURN ANGLE");
  DEBUG_PRINT(right);
  DEBUG_PRINTLN(angle);

  if (right == false) {
    position_old = RightEnc.read();
  } else {
    position_old = LeftEnc.read();
  }

  long int stop_pos;
  long int delta;
  int delta_prev = tick_per_rot; // at max angle = 360° and delta is the numbner of tick per turn
  bool turn_loop = true;


  if (right == false) {
    // turn left
    stop_pos = position_old - angle_to_tick(angle);
  } else {
    //turn right
    stop_pos = position_old + angle_to_tick(angle);
  }

  do {
    soft_turn(right, middle_speed);
    if (right == false) {
      position_ = LeftEnc.read();
    } else {
      position_ = RightEnc.read();
    }
    delta = position_ - stop_pos;

    if (delta < 120) {
      turn_loop = false;
    }

  } while (turn_loop == true);
  stop_motor_smooth();
}

/* REMOTE */

void forward_smooth_remote() {
  RDEBUG_PRINTLN("1 DIR FALSE");
#ifdef MATRIX
  matrix.clear();
  matrix.drawBitmap(0, 0, forward_bmp, 8, 8, LED_GREEN);
  matrix.writeDisplay();
#endif
  // forward only if we were stopped. Else, juste continue
  if (moving == false) {
    forward(30, 100);
    moving = true;
  }
}

void backward_smooth_remote() {
  if (moving == true) {
    // we are moving forward, we need to stop before going backward
    if (dir == false) {
      stop_motor_smooth();
    }
  }
  backward(30, 100);
  moving = true;
#ifdef MATRIX
  matrix.clear();
  matrix.drawBitmap(0, 0, backward_bmp, 8, 8, LED_RED);
  matrix.writeDisplay();
#endif
}

void hard_turn_remote(bool right) {
  hard_turn(right, middle_speed);
}

void soft_turn_remote(bool right) {
  soft_turn(right, middle_speed);
}
