/*
  # @author: Arnaud Joset
  #
  # This file is part of Agayon: R1D3.
  #
  # R1D3 is free software: you can redistribute it and/or modify
  # it under the terms of the GNU General Public License as published by
  # the Free Software Foundation, either version 3 of the License, or
  # (at your option) any later version.
  #
  # R1D3 is distributed in the hope that it will be useful,
  # but WITHOUT ANY WARRANTY; without even the implied warranty of
  # MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  # GNU General Public License for more details.
  #
  # You should have received a copy of the GNU General Public License
  # along with R1D3.  If not, see <http://www.gnu.org/licenses/>.
  #
*/

void types(String a) {
  DEBUG_PRINT("it's a String");
}
void types(int a)   {
  DEBUG_PRINT("it's an int");
}
void types(char* a) {
  DEBUG_PRINT("it's a char*");
}
void types(float a) {
  DEBUG_PRINT("it's a float");
}

void mode_printer() {
  RPI_PRINT("MODE = ");
  RPI_PRINTLN(mode);
}

void encoder_manager() {

  if (moving == true) {
    // We are supposed to move but the encoder do not change. We must be stuck.
    stop_motor();
#ifdef MATRIX
    matrix.clear();
    matrix.drawBitmap(0, 0, error_bmp, 8, 8, LED_RED);
    matrix.writeDisplay();  // write the changes we just made to the display
#endif
  }
  // Read encoders. Be ready to send the values
  position_right = RightEnc.read();
  if (position_right != position_right_old) {
    position_right_old = position_right;
    //counter = 0;
  }

  position_left = LeftEnc.read();
  if (position_left != position_left_old) {
    position_left_old = position_left;
    //counter = 0;
  }
  RPI_PRINT(",EN:er_");
  RPI_PRINT(position_right);
  RPI_PRINT(":el_");
  RPI_PRINTLN(position_left);
}


void minimu_manager_avg(int delta) {
  long unsigned int stop_time = millis() + delta;
  long unsigned int timer_tmp;
  // FIXME todo more set a timer in the future
  do {
    timer_tmp = millis();
    counter = 0;
    if ((timer_tmp - timer_minimu) >= 20) // Main loop runs at 50Hz
    {
      counter++;
      if (timer_tmp > timer_minimu)
        G_Dt = (timer - timer_minimu) / 1000.0; // Real time of loop run. We use this on the DCM algorithm (gyro integration time)
      else
        G_Dt = 0;

      // *** DCM algorithm
      // Data adquisition
      Read_Gyro();   // This read gyro data
      Read_Accel();     // Read I2C accelerometer

      if (counter > 5)  // Read compass data at 10Hz... (5 loop runs)
      {
        counter = 0;
        Read_Compass();    // Read I2C magnetometer
        Compass_Heading(); // Calculate magnetic heading
      }

      // Calculations...
      Matrix_update();
      Normalize();
      Drift_correction();
      Euler_angles();
      // ***
      minimu_to_RPI();
    }
  } while (timer_tmp < stop_time);
}

float complete_rotations(int delta_tick) {
  return delta_tick / tick_per_rot;
}

float ongoing_angle(int tick) {
  float rotations = complete_rotations(tick);
  int integer_part = (int)(rotations);
  float decimal_part = (rotations - integer_part);

  return decimal_part * 360;
}

int angle_to_tick(int angle) {
  /* First we calculate the fraction of the angle.
    fractions < 1 are weird with arduino so we calculate the inverse.
  */
  int ticks = 360 / angle;
  ticks = tick_per_rot / ticks;
  return (int)(ticks);
}

void sensors_to_rpi() {

#ifdef MATRIX
  matrix.clear();
  matrix.drawBitmap(0, 0, obstacles_bmp, 8, 8, LED_YELLOW);
  matrix.writeDisplay();
#endif
  delay(300);
  RPI_PRINT(",CU:ac_");
  RPI_PRINT(timer);
  RPI_PRINT(",EN:er_");
  RPI_PRINT(position_right);
  RPI_PRINT(":el_");
  RPI_PRINT(position_left);
  check_sides(true);
  delay(30);
  check_front(true);
  delay(30);
  check_back(true);
  //minimu_manager_avg(5000);
  delay(30);
  RPI_PRINTLN(",CU:ac_-1");
  // n second timeout for RPI
  rpi_timeout = millis() + 5000;
}
